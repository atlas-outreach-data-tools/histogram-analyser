Hi!

here is the small set of files with the code and the needed library to run the histogram app called<br>
[Higgs to WW - simulated data](http://opendata.atlas.cern/visualisations/analyser-js.php) and the [Top rare processes analyser](http://opendata.atlas.cern/release/2020/documentation/visualization/histogram-analyser-ttZ.html)

The two kind of files that you can explore, are *.html and *.csv
Where files ending in `.html` are (mainly) the one to study and hack :)

for feedback, please use one of our channels in:<br>
http://opendata.atlas.cern/community/contact.php
</br>
